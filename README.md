# README #

In questo repository sono inclusi i files necessari per la visualizzazione dei monitoraggi treni tramite la piattaforma trenipendolari.it

Non vengono inclusi tutti gli scripts Perl necessari per la gestione del database MySQL incluse tutte le query per interrogare il sistema centrale delle ferrovie.

La piattaforma è costituita da scripts scritti in Perl che lavorano lato server verso il server di Trenitalia per l'acquisizione dei dati di percorrenza dei vari
treni e da una serie di scripts Perl per inserire questi dati in un database MySQL che gira in locale sul server. I dati estratti dal sito delle ferrovie sono in
formato JSON, da cui è possibile estrapolare quasi ogni info in tempo reale circa la circolazione a livello nazionale dei vari treni, quali il punto dove il treno
è stato rilevato l'ultima volta, il ritardo accumulato e le eventuali soppressioni, totali o parziali.
La piattaforma esegue diverse query al minuto, mediante chiamate lanciate in parallelo e raccoglie tutti i dati prima di elaborarli per poi inserirli nel DB.
Esiste anche un archivio storico che mantiene lo stato di viaggio dei vari treni nel corso dell'anno, in modo che sia possibile analizzare l'andamento delle varie
direttrici nel corso dell'anno. 
Infine c'è la parte di visualizzazione che consente all'utente di poter visionare lo stato dei treni in tempo reale; al fine di migliorare il servizio sono stati
introdotti diversi miglioramenti di tanto in tanto ed in questo momento è in corso un aggiornamento per poter pubblicare i dati di circolazione con la tecnica "push"
senza dover fare il refresh di pagina per vedere l'aggiornamento.
A tal scopo in questo repository sono inseriti solo ed unicamente i files relativi a questa parte dal momento che è in atto una collaborazione con uno sviluppatore
che conosce JavaScript, usato in questa fase per estrarre i dati prodotti da uno script Perl che esegue la query al DB e presenta il flusso dati in tempo reale in
formato JSON per poi essere formattati e impaginati in JavaScript.

# Contributi
Per la parte JavaScript si ringrazia Federico Almaviva che sta attivamente creando il codice necessario alla gestione dei dati e la loro impaginazione nella pagina
HTML partendo dal JSON che viene fornito dal Perl che esegue le query al DB.
