#!/usr/bin/perl

################## LED #####################
# 0=gray.png
# 1=red_on.png
# 2=red_flashing.gif
# 3=yellow_on.png
# 4=yellow_flashing.gif
# 5=green_on.png
# 6=green_flashing.gif
# 7=blue_on.png
# 8=orange_on.png
# 9=orange_flashing.gif
############################################

################ STATUS ####################
# 0=no run
# 1=cancelled
# 2=not valid
# 3=running
# 4=arrived
############################################

use Time::Local;
use strict;
use DBI;

my $db_host='localhost';
my $db_user='username';
my $db_pass='password';
my $db_name='dashboard';
my $dblink=DBI->connect("DBI:mysql:$db_name:$db_host", $db_user, $db_pass);
my $sql_read2='';
my $sql_read5='';
my @elem1='';
my @elem2='';
my @elem3='';
my @elem4='';
my @elem5='';
my $LED_PATH='';
my $direttrice='';
my $cnt=0;
my $cntOrario=0;
my $cntAnticipo=0;
my $cntRitardo=0;
my $cntNoRun=0;
my $cntCanc=0;
my $TotRit=0;
my $cntRun=0;
my $media=0;
my $min=0;
my $TIMESTAMP='';
my @OPTION='';

my $sql_read=$dblink->prepare("SELECT * FROM `DIRETTRICI` WHERE `SEGNALA` <> '0' ORDER BY `INDEX`;");
$sql_read->execute();

while(@elem1=$sql_read->fetchrow_array()) {
        my $DIR_ID=$elem1[0];
        my $DESCR_ID=$elem1[5];
        push(@OPTION, ("<option value=\"$DIR_ID\">$DIR_ID:$DESCR_ID</option>"));
}

my $sql_read1=$dblink->prepare("SELECT * FROM `DIRETTRICI` WHERE `DIR` = '$ARGV[0]' AND `SEGNALA` <> '0';");
$sql_read1->execute();

while(@elem2=$sql_read1->fetchrow_array()) {
        my $DIR_ID=$elem2[0];
        my $DESCR_ID=$elem2[1];
        $direttrice="$DIR_ID:$DESCR_ID";
}

my $home="<td height=\"45\" align=\"center\" nowrap=\"nowrap\" class=\"navText\"><a href=\"http://www.trenipendolari.it/index.html\"><strong>Home</strong></a></td>";
my $info="<td height=\"45\" align=\"center\" nowrap=\"nowrap\" class=\"navText\"><a href=\"http://www.trenipendolari.it/info.html\"><strong>Info</strong></a></td>";
my $status="<td height=\"45\" align=\"center\" nowrap=\"nowrap\" class=\"navText\"><a href=\"http://www.trenipendolari.it/cgi-bin/status\"><strong>Stato Treni</strong></a></td>";
my $status_navi="<td height=\"45\" align=\"center\" nowrap=\"nowrap\" class=\"navText\"><a href=\"http://www.trenipendolari.it/cgi-bin/ship\"><strong>Stato Traghetti RFI</strong></a></td>";
my $arch="<td height=\"45\" align=\"center\" nowrap=\"nowrap\" class=\"navText\"><a href=\"http://www.trenipendolari.it/cgi-bin/storico\"><strong>Storico Treni</strong></a></td>";
my $t01="<form action=\"/cgi-bin/status-new\" method=get>
<select name=\"direttrice\">
<option value=\"\">Scegli la DIR e premi OK</option>
@OPTION
</select><input TYPE=\"SUBMIT\" VALUE=\"OK\"></form>";
my $t02="<td height=\"45\" align=\"center\" class=\"pageName\">$direttrice</td>";
my $t05="<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");</script>";
my $t06='<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.min.js"></script>';
my $t07='<script type="text/javascript" src="http://www.trenipendolari.it/status.js"></script>';
my $t08="<script type=\"text/javascript\">
var allData=\"\"
var allNewData=\"\"

function invokeSSE(){
        var source = new EventSource('http://www.trenipendolari.it/cgi-bin/push-status.pl?$ARGV[0]');

        source.addEventListener('server-treno', function(a) {
                allNewData=JSON.parse(a.data)
                if(allData==\"\"){
                        allData=allNewData
                        forceUpdate=true;
                }
                doStuff()
        }, false);

        source.addEventListener('open', function(a) {
        //Connection Open
        }, false);

        source.addEventListener('error', function(a) {
                if(a.readyState == EventSource.CLOSED) {
                        alert(\"Connection closed\");
                }
        }, false);
}
</script>";

print("<!doctype html>");
print("<html>");
print("<head>");
print("   <meta charset=\"UTF-8\">");
print("   <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0 , maximum-scale=1.0, user-scalable=no\" />");
print("   <meta name=\"description\" content=\"Monitoraggio treni pendolari, ritardi e soppressioni, linea per linea\">");
print("   <meta name=\"keyword\" content=\"html, treni, pendolari, ritardi, linee ferroviarie, cancellazioni treno\">");
print("   <meta name=\"author\" content=\"Armando Accardo\">");
print("   <title>Rilevamento Live Treni</title>");
print("   <link rel=\"stylesheet\" href=\"http://www.trenipendolari.it/status.css\" type=\"text/css\" />");
print("   <style type=\"text/css\">");
print("   <!--");
print("   .style1 {font-size: 14px}");
print("   -->");
print("   </style>");
print("$t05");
print("$t06");
print("$t07");
print("$t08");
print("</head>");
print("<body background=\"http://www.trenipendolari.it/images/tabellone_bkgnd.jpg\" onload=\"invokeSSE()\"><center>");

print("<table width=580 border=0 cellspacing=0 cellpadding=0>");
print("<tr>");
print("<td>");

print("<center><table width=500 border=0 cellspacing=0 cellpadding=0 id=\"navigation\">");
print("<tr>");
print("$home");
print("$info");
print("$status");
print("$status_navi");
print("$arch");
print("</tr></table></center>");

print("<p>&nbsp");
print("<hr><center><table width=\"500\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
print("$t01");
print("</table></center>");
print("<hr>");
print("</p>");
print("&nbsp<br>");

print("<center><table width=580 border=0 cellspacing=0 cellpadding=0>");
print("<tr>");
print("$t02");
print("</tr>");
print("</table></center>");

my $t03="<tr bgcolor=\"#D3DCE6\"><th width=\"50\" align=\"center\" valign=\"middle\"><span class=\"style1\">Treno</span></th><th width=\"20\" align=\"center\" valign=\"middle\"><span class=\"style1\">Stato</span></th><th width=\"80\" align=\"center\" valign=\"middle\"><span class=\"style1\">Ritardo</span></th><th width=\"200\" align=\"center\" valign=\"middle\"><span class=\"style1\">Info</span></th></tr>";
my $t04D7="<a class=\"twitter-timeline\" href=\"https://twitter.com/TRENORD_miTI\" data-widget-id=\"487160435905294337\">Tweets by TRENORD_miTI</a>";
my $t04D22="<a class=\"twitter-timeline\" href=\"https://twitter.com/TRENORD_miTT\" data-widget-id=\"476277549916356609\">Tweets by TRENORD_miTT</a>";
my $t04D23="<a class=\"twitter-timeline\" href=\"https://twitter.com/TRENORD_miPCs\" data-widget-id=\"476278790096556032\">Tweets by TRENORD_miPCs</a>";
my $t04D24="<a class=\"twitter-timeline\" href=\"https://twitter.com/TRENORD_pvCD\" data-widget-id=\"476279630563786752\">Tweets by TRENORD_pvCD</a>";
my $t04D25="<a class=\"twitter-timeline\" href=\"https://twitter.com/TRENORD_miAL\" data-widget-id=\"476280254349058048\">Tweets by TRENORD_miAL</a>";
my $t04D26="<a class=\"twitter-timeline\" href=\"https://twitter.com/TRENORD_alLUI\" data-widget-id=\"476281382449078272\">Tweets by TRENORD_alLUI</a>";
my $t04D27="<a class=\"twitter-timeline\" href=\"https://twitter.com/TRENORD_alPV\" data-widget-id=\"476273170236715008\">Tweets by TRENORD_alPV</a>";
my $t04D28="<a class=\"twitter-timeline\" href=\"https://twitter.com/TRENORD_vcPV\" data-widget-id=\"476274964308312064\">Tweets by TRENORD_vcPV</a>";
my $t04D28P="<a class=\"twitter-timeline\" href=\"https://twitter.com/PendolariVCPV\" data-widget-id=\"476322732901011456\">Tweets by PendolariVCPV</a>";
my $t04S5="<a class=\"twitter-timeline\" href=\"https://twitter.com/TRENORD_miVC\" data-widget-id=\"476283945185599488\">Tweets by TRENORD_miVC</a>";
my $t04S6="<a class=\"twitter-timeline\" href=\"https://twitter.com/TRENORD_miVC\" data-widget-id=\"476283945185599488\">Tweets by TRENORD_miVC</a>";
my $t04S13="<a class=\"twitter-timeline\" href=\"https://twitter.com/TRENORD_miTT\" data-widget-id=\"476277549916356609\">Tweets by TRENORD_miTT</a>";

if($ARGV[0]) {
        print("<center><table id=\"output\" width=\"90%\" border=1 align=\"center\" cellspacing=2 cellpadding=0>");
        print("$t03");
        $sql_read2=$dblink->prepare("SELECT * FROM `TRENI` WHERE `DIR` = '$ARGV[0]' AND `STATO` <> '2' AND `SEGNALA` <> '0' ORDER BY `ORA_PAR`;");
        $sql_read2->execute();

        while(@elem3=$sql_read2->fetchrow_array()) {
                my $STATO=$elem3[8];
                my $RIT=$elem3[10];
                $cnt++;

                if(($STATO == 3 || $STATO == 4) && $RIT == 0) {
                        $cntOrario++;
                }

                if(($STATO == 3 || $STATO == 4) && $RIT < 0) {
                        $cntAnticipo++;
                }

                if(($STATO == 3 || $STATO == 4) && $RIT > 0) {
                        $cntRitardo++;
                }

                if($STATO == 0 || $STATO == 2) {
                        $cntNoRun++;
                }

                if($STATO == 1) {
                        $cntCanc++;
                }
        }

        my $sql_read3=$dblink->prepare("SELECT SUM(`RIT`) AS TOTAL FROM `TRENI` WHERE `DIR` = '$ARGV[0]' AND `RIT` > '0';");
        $sql_read3->execute();

        while(@elem4=$sql_read3->fetchrow_array()) {
                $TotRit=$elem4[0];
        }

        $cntRun=$cnt-$cntNoRun-$cntCanc;

        if($cntRun != 0) {
                $media=$TotRit/$cntRun;
                $min=int($media+0.5);
                $media=sprintf("%.1d min", $min);
        } else {
                $media=0;
        }

        my $sql_read4=$dblink->prepare("SELECT `TIMESTAMP` FROM `TRENI` WHERE `DIR` = '$ARGV[0]' ORDER BY `TIMESTAMP` DESC LIMIT 1;");
        $sql_read4->execute();

        while(@elem5=$sql_read4->fetchrow_array()) {
                $TIMESTAMP=$elem5[0];
        }

        print("</table></center>");
        print("<p>&nbsp");
        print("<hr>");
        print("</p>");
        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Statistiche finali:</td></tr>");

        if($cntNoRun == 0) {
                if($cntCanc == 0) {
                        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Totale $cnt treni: <font color=\"#266A00\">$cntOrario in orario</font> | <font color=\"#266A00\">$cntAnticipo in anticipo</font> | $cntRitardo in ritardo<br />");
                } elsif($cntCanc == 1) {
                        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Totale $cnt treni: <font color=\"#266A00\">$cntOrario in orario</font> | <font color=\"#266A00\">$cntAnticipo in anticipo</font> | $cntRitardo in ritardo | <font color=\"#FF0000\">$cntCanc cancellato</font><br />");
                } else {
                        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Totale $cnt treni: <font color=\"#266A00\">$cntOrario in orario</font> | <font color=\"#266A00\">$cntAnticipo in anticipo</font> | $cntRitardo in ritardo | <font color=\"#FF0000\">$cntCanc cancellati</font><br />");
                }
        } elsif($cntNoRun == 1) {
                if($cntCanc == 0) {
                        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Totale $cnt treni: <font color=\"#266A00\">$cntOrario in orario</font> | <font color=\"#266A00\">$cntAnticipo in anticipo</font> | $cntRitardo in ritardo | $cntNoRun non ha ancora viaggiato<br />");
                } elsif($cntCanc == 1) {
                        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Totale $cnt treni: <font color=\"#266A00\">$cntOrario in orario</font> | <font color=\"#266A00\">$cntAnticipo in anticipo</font> | $cntRitardo in ritardo | <font color=\"#FF0000\">$cntCanc cancellato</font> | $cntNoRun non ha ancora viaggiato<br />");
                } else {
                        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Totale $cnt treni: <font color=\"#266A00\">$cntOrario in orario</font> | <font color=\"#266A00\">$cntAnticipo in anticipo</font> | $cntRitardo in ritardo | <font color=\"#FF0000\">$cntCanc cancellati</font> | $cntNoRun non ha ancora viaggiato<br />");
                }
        } else {
                if($cntCanc == 0) {
                        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Totale $cnt treni: <font color=\"#266A00\">$cntOrario in orario</font> | <font color=\"#266A00\">$cntAnticipo in anticipo</font> | $cntRitardo in ritardo | $cntNoRun non hanno ancora viaggiato<br />");
                } elsif($cntCanc == 1) {
                        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Totale $cnt treni: <font color=\"#266A00\">$cntOrario in orario</font> | <font color=\"#266A00\">$cntAnticipo in anticipo</font> | $cntRitardo in ritardo | <font color=\"#FF0000\">$cntCanc cancellato</font> | $cntNoRun non hanno ancora viaggiato<br />");
                } else {
                        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Totale $cnt treni: <font color=\"#266A00\">$cntOrario in orario</font> | <font color=\"#266A00\">$cntAnticipo in anticipo</font> | $cntRitardo in ritardo | <font color=\"#FF0000\">$cntCanc cancellati</font> | $cntNoRun non hanno ancora viaggiato<br />");
                }
        }

        print("$cntRun treni hanno totalizzato $TotRit min di ritardo | $media di ritardo medio<br />");
        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Ultimo rilevamento da viaggiatreno il $TIMESTAMP</td></tr>");
        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Disponibile il gruppo FaceBook <a href=\"https://www.facebook.com/groups/TreniPendolari.it/\" target=\"_blank\">TreniPendolari.it</a> di supporto al sito</td></tr>");
        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">&copy; 2013-2016 Armando Accardo - Coordinamento Provinciale Pendolari Pavesi</td></tr>");

if($ARGV[0] eq "D7") {
                print("<tr><td align=\"center\"><hr></td></tr>");
                print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Cosa dice Trenord:</td></tr>");
                print("<tr><td align=\"center\">$t04D7</td></tr>");
        } elsif($ARGV[0] eq "D22") {
                print("<tr><td align=\"center\"><hr></td></tr>");
                print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Cosa dice Trenord:</td></tr>");
                print("<tr><td align=\"center\">$t04D22</td></tr>");
        } elsif($ARGV[0] eq "D23") {
                print("<tr><td align=\"center\"><hr></td></tr>");
                print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Cosa dice Trenord:</td></tr>");
                print("<tr><td align=\"center\">$t04D23</td></tr>");
        } elsif($ARGV[0] eq "D24") {
                print("<tr><td align=\"center\"><hr></td></tr>");
                print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Cosa dice Trenord:</td></tr>");
                print("<tr><td align=\"center\">$t04D24</td></tr>");
        } elsif($ARGV[0] eq "D25") {
                print("<tr><td align=\"center\"><hr></td></tr>");
                print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Cosa dice Trenord:</td></tr>");
                print("<tr><td align=\"center\">$t04D25</td></tr>");
        } elsif($ARGV[0] eq "D26") {
                print("<tr><td align=\"center\"><hr></td></tr>");
                print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Cosa dice Trenord:</td></tr>");
                print("<tr><td align=\"center\">$t04D26</td></tr>");
        } elsif($ARGV[0] eq "D27") {
                print("<tr><td align=\"center\"><hr></td></tr>");
                print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Cosa dice Trenord:</td></tr>");
                print("<tr><td align=\"center\">$t04D27</td></tr>");
        } elsif($ARGV[0] eq "D28") {
                print("<tr><td align=\"center\"><hr></td></tr>");
                print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Cosa dice Trenord:</td></tr>");
                print("<tr><td align=\"center\">$t04D28</td></tr>");
        } elsif($ARGV[0] eq "S5") {
                print("<tr><td align=\"center\"><hr></td></tr>");
                print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Cosa dice Trenord:</td></tr>");
                print("<tr><td align=\"center\">$t04S5</td></tr>");
        } elsif($ARGV[0] eq "S6") {
                print("<tr><td align=\"center\"><hr></td></tr>");
                print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Cosa dice Trenord:</td></tr>");
                print("<tr><td align=\"center\">$t04S6</td></tr>");
        } elsif($ARGV[0] eq "S13") {
                print("<tr><td align=\"center\"><hr></td></tr>");
                print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Cosa dice Trenord:</td></tr>");
                print("<tr><td align=\"center\">$t04S13</td></tr>");
        }

        print("</td></tr></table>");
        print("</center>");
        print("</body>");
        print("</html>");
} else {
print("<tr><td height=\"50\" align=\"center\" class=\"style1\">Disponibile il gruppo FaceBook <a href=\"https://www.facebook.com/groups/TreniPendolari.it/\" target=\"_blank\">TreniPendolari.it</a> di supporto al sito</td></tr>");
        print("<tr><td height=\"50\" align=\"center\" class=\"style1\">&copy; 2013-2016 Armando Accardo - Coordinamento Provinciale Pendolari Pavesi</td></tr>");
        print("</td></tr></table>");
        print("</center></body>");
        print("</html>");
}

$dblink->disconnect();
