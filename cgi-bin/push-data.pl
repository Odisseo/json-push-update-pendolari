#!/usr/bin/perl

use strict;
use DBI;

my $db_host='localhost';
my $db_user='username';
my $db_pass='password';
my $db_name='dashboard';
my $dblink=DBI->connect("DBI:mysql:$db_name:$db_host", $db_user, $db_pass);
my @elem='';
my $LED_PATH='';

my $sql_read=$dblink->prepare("SELECT * FROM `TRENI` WHERE `DIR` = '$ARGV[0]' AND `STATO` <> '2' AND `SEGNALA` <> '0' ORDER BY `ORA_PAR`;");
$sql_read->execute();

my $number=$sql_read->rows();
my $i=1;

print("Content-Type: text/event-stream\n\n");
print("Cache-Control: no-cache\n\n");
print("Connection: keep-alive\n\n");
print("event: server-treno\n");
print("data: {\n");

while(@elem=$sql_read->fetchrow_array()) {
        my $TRENO=@elem[1];
        my $S1=$elem[3];
        my $P=$elem[4];
        my $S2=$elem[6];
        my $A=$elem[7];
        my $STATO=$elem[8];
        my $LED=@elem[9];
        my $RIT=@elem[10];
        my $INFO=@elem[11];

        if($LED == 0) {
                $LED_PATH="http://www.trenipendolari.it/images/gray.png";
        } elsif($LED == 1) {
                $LED_PATH="http://www.trenipendolari.it/images/red_on.png";
        } elsif($LED == 2) {
                $LED_PATH="http://www.trenipendolari.it/images/red_flashing.gif";
        } elsif($LED == 3) {
                $LED_PATH="http://www.trenipendolari.it/images/yellow_on.png";
        } elsif($LED == 4) {
                $LED_PATH="http://www.trenipendolari.it/images/yellow_flashing.gif";
        } elsif($LED == 5) {
                $LED_PATH="http://www.trenipendolari.it/images/green_on.png";
        } elsif($LED == 6) {
                $LED_PATH="http://www.trenipendolari.it/images/green_flashing.gif";
        } elsif($LED == 7) {
                $LED_PATH="http://www.trenipendolari.it/images/blue_on.png";
        } elsif($LED == 8) {
                $LED_PATH="http://www.trenipendolari.it/images/orange_on.png";
        } elsif($LED == 9) {
                $LED_PATH="http://www.trenipendolari.it/images/orange_flashing.gif";
        }

        if($i < $number) {
                print("data: \"R$TRENO\": [{\"S1\": \"$S1\"},{\"P\": \"$P\"},{\"S2\": \"$S2\"},{\"A\": \"$A\"},{\"LED_PATH\": \"$LED_PATH\"},{\"RIT\": \"$RIT\"},{\"STATO\": \"$STATO\"},{\"INFO\": \"$INFO\"}],\n");
        } else {
                print("data: \"R$TRENO\": [{\"S1\": \"$S1\"},{\"P\": \"$P\"},{\"S2\": \"$S2\"},{\"A\": \"$A\"},{\"LED_PATH\": \"$LED_PATH\"},{\"RIT\": \"$RIT\"},{\"STATO\": \"$STATO\"},{\"INFO\": \"$INFO\"}]\n");
        }

        $i ++;
}

print("data: }\n\n");

$dblink->disconnect();
